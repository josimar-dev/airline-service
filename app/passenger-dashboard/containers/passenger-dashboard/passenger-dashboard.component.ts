import { Component, OnInit } from '@angular/core';
import  { Passenger } from '../../models/passenger.interfaces'
import { DashboardService } from '../../passenger-dashboard.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'passenger-dashboard',
  styleUrls: ['passenger-dashboard.component.scss'],
  templateUrl: 'passenger-dashboard.component.html'
})

export class PassengerDashboardComponent implements OnInit {
  passengers: Passenger[];
  constructor(private dashboardService: DashboardService) {}
  ngOnInit() {
    this.dashboardService.getPassengers().subscribe((data: Passenger[]) => this.passengers = data);
  }

  handleRemove(event: Passenger) {
    this.dashboardService
      .removePassenger(event)
      .subscribe((data: Passenger) => {
        this.passengers = this.passengers.filter((passenger: Passenger) => passenger.id !== event.id);
      })
  }

  handleEdit(event: Passenger) {
    this.dashboardService
      .updatePassenger(event)
      .subscribe((data: Passenger) => {
        console.log('DATA...', data);
        this.passengers = this.passengers.map((passenger: Passenger) => {
          if (passenger.id === event.id) {
            passenger = Object.assign({}, passenger, event);
          }
          return passenger;
        });     
      })
  }
}